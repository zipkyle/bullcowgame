//
// Created by Kyle Scott on 12/5/2018.
//
#pragma once
#include <string>

using FString = std::string;
using int32 = int;

struct FBullCowCount {
    int32 Bulls = 0;
    int32 Cows = 0;

};

enum class EguessStatus {
    OK,
    Not_Isogram,
    Wrong_Length,
    Invalid_Status,
    Not_Lowercase
};




class FBullCowGame {
public:
    FBullCowGame(); //Constructor
    int32 GetMaxTries() const;
    int32 GetCurrentTry() const;
    int32 GetHiddenWordLength() const;
    bool IsGameWon() const;

    void Reset();
    EguessStatus CheckGuessValidity(FString) const;
    FBullCowCount SubmitGuess(FString);



private:

    // See Constructor for initialization
    int32 MyCurrentTry;
    FString MyHiddenWord;
    bool bGameIsWon;
    bool IsIsogram(FString) const;
    bool IsUpercase(FString) const;

};
