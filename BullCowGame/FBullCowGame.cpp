//
// Created by Kyle Scott on 12/6/2018.
//
#include "FBullCowGame.h"
#include <map>
#define TMap std::map
#pragma once

using int32 = int;
//default constructor
FBullCowGame::FBullCowGame() {
    Reset();
}
int32 FBullCowGame::GetMaxTries() const {
    TMap<int32, int32> WordLengthToMaxTires {
            {3,4},
            {4,8},
            {5,10},
            {6,12}
    };
    return WordLengthToMaxTires[MyHiddenWord.length()];
}

void FBullCowGame::Reset() {

    const FString HIDDEN_WORD = "brick";
    MyHiddenWord = HIDDEN_WORD;
    MyCurrentTry = 1;
    bGameIsWon = false;

    return;
}

int32 FBullCowGame::GetCurrentTry() const {
    return MyCurrentTry;
}

bool FBullCowGame::IsGameWon() const {return bGameIsWon;}

EguessStatus FBullCowGame::CheckGuessValidity(FString guess) const {
    // if guess isn't an isogram return an error
    if (!IsIsogram(guess)) {
        return EguessStatus::Not_Isogram;
    }
    if (GetHiddenWordLength() != guess.length()) {
            return EguessStatus::Wrong_Length;

    }
    if (IsUpercase(guess)) {
        return EguessStatus::Not_Lowercase;
    }
    return EguessStatus::OK;

}
//Recieves a valid guess, increments turn, and returns count.
FBullCowCount FBullCowGame::SubmitGuess(FString guess) {

    MyCurrentTry++;
    FBullCowCount BullCowCount;

    // loop through all letters in the hidden word
    for (int32 MHWChar = 0; MHWChar < MyHiddenWord.length(); MHWChar++) {
        // compare letters against the guess
        for (int32 GChar = 0; GChar < MyHiddenWord.length(); GChar++) {
        //if they match then
        if (guess[MHWChar] == MyHiddenWord[GChar]) {
            if (guess[MHWChar] == MyHiddenWord[MHWChar]) {
                BullCowCount.Bulls++;
            } else {
                BullCowCount.Cows++;
            }

        }
    }
    }
    if (BullCowCount.Bulls == MyHiddenWord.length()) {
        bGameIsWon = true;
    } else {
        bGameIsWon = false;
    }

    return BullCowCount;
}

int32 FBullCowGame::GetHiddenWordLength() const {return MyHiddenWord.length();}

bool FBullCowGame::IsIsogram(FString Word) const {

    //treat 0 and 1 letter words as isograms
    if (Word.length() <= 1) { return true;}

    //setup our map
    TMap <char, bool> LetterSeen;
    for (auto Letter : Word) { //for all letters of the word
        Letter = tolower(Letter);
        if (LetterSeen[Letter]) {return false;} else {
            LetterSeen[Letter] = true;
        }

    }

    //loop through letter all the letters of the word
      //if the letter is in the map
        //we do not have an isogram
    // otherwise
        //add the letter ot the map as seen
    return true; // for example in case where /0 is entered
}

bool FBullCowGame::IsUpercase(FString Word) const {
    for (auto Letter: Word) {
        if (isupper(Letter)) { return true;}
    }

    return false;
}

