/* This is the console executable that makes use of the BullCow Class
 * This acts as the view in a MVC pattern, and is responsible for all user interaction.
 * For game logic see the FBullCowGame Class.
 */

#include <iostream>
#include <string>
#include "FBullCowGame.h"
#pragma once

using FText = std::string; 
using int32 = int; 

// function prototypes as outside of a class
void PrintIntro();
void PrintGameSummary();
void PlayGame();
FText GetValidGuess();
bool AskToPlayAgain();

FBullCowGame BCGame; //initialize a new game, which we reuse across plays
//Application Entry Point
int main() {
	bool bPlayAgain = false;
	do {
		PrintIntro();
		PlayGame();
		bPlayAgain = AskToPlayAgain();
	} while (bPlayAgain);
	return 0;
	
}

void PlayGame() {
    BCGame.Reset();
    //loop asking for guesses while the game is NOT won
    // and there are still tries remaining

	while (!BCGame.IsGameWon() && BCGame.GetCurrentTry() <= BCGame.GetMaxTries()) {
		 FText guess = GetValidGuess();

		 // Submit valid guess to the game
		FBullCowCount BullCowCount = BCGame.SubmitGuess(guess);

		 std::cout << "Bulls: " << BullCowCount.Bulls << " ";
		 std::cout << "Cows: " << BullCowCount.Cows << std::endl;
	}

	PrintGameSummary();
	return;
}

void PrintIntro() {
	std::cout << "Welcome to Bulls and Cows, a fun word game.\n";
	std::cout << std::endl;
	std::cout << "          }   {            ___ " << std::endl;
	std::cout << "          (o o)           (o o) " << std::endl;
	std::cout << "   /-------\\ /    and      \\ /-------\\ " << std::endl;
	std::cout << "  / | BULL |O               O| COW  | \\ " << std::endl;
	std::cout << " *  |-,--- |                |------|  * " << std::endl;
	std::cout << "    ^      ^                 ^      ^ " << std::endl;
	std::cout << "Can you guess the " << BCGame.GetHiddenWordLength() << " letter isogram I'm thinking of?";
	std::cout << std::endl;
}

FText GetValidGuess() {
    EguessStatus Status = EguessStatus::Invalid_Status;
    FText Guess = " ";
    do {
        std::cout << std::endl << BCGame.GetCurrentTry() << "/" << BCGame.GetMaxTries() << " Response: ";
        getline(std::cin, Guess);
       Status = BCGame.CheckGuessValidity(Guess);

        switch (Status) {
            case EguessStatus::Wrong_Length:
                std::cout << "Please enter a " << BCGame.GetHiddenWordLength() << " letter word.\n";
                break;
            case EguessStatus::Not_Isogram:
                std::cout << "Please enter a valid Isogram\n";
                std::cout << "An Isogram is a word without duplicate letters";
                break;
			case EguessStatus::Not_Lowercase:
				std::cout << "Please only use lowercase letters\n";
				break;
            default: //assume valid
                break;
        }
    } while (Status != EguessStatus::OK); //keep looping until no errors
    return Guess;
}

bool AskToPlayAgain() {
	std::cout << "Do you want to play again with the same word?(y/n)";
		FText Response = " ";
		getline(std::cin, Response);
	return (Response[0] == 'y') || (Response[0] == 'Y');

}

void PrintGameSummary() {
	if (BCGame.IsGameWon()) {
		std::cout << "Congratulations! You have beat the game!!!!!\n\n";
	} else {
		std::cout << "Out of guesses! Game over....\n\n";

	}
}
